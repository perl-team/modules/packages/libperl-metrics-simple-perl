Source: libperl-metrics-simple-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libjson-pp-perl <!nocheck>,
                     libppi-perl <!nocheck>,
                     libreadonly-perl <!nocheck>,
                     libstatistics-basic-perl <!nocheck>,
                     libtest-compile-perl <!nocheck>,
                     libtest-pod-coverage-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libperl-metrics-simple-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libperl-metrics-simple-perl.git
Homepage: https://metacpan.org/release/Perl-Metrics-Simple
Rules-Requires-Root: no

Package: libperl-metrics-simple-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libjson-pp-perl,
         libppi-perl,
         libreadonly-perl,
         libstatistics-basic-perl
Description: module to provide metrics of Perl code
 Perl::Metrics::Simple is a Perl module that provides several useful methods
 for static analysis of one or many Perl files. It currently offers these
 metrics: packages, subroutines, lines of code, and an approximation of
 cyclomatic (mccabe) complexity for the subroutines and the "main" portion
 of the code.
 .
 This module is designed to be simpler than the similar Perl::Metric module.
